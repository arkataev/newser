<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Requests\ArticlePostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ArticleController extends Controller
{

    public function postArticle(ArticlePostRequest $request)
    {
		$data = $request->all();
	    // если в запросе есть параметр id, достать из БД статью с этим id
	    $article = isset($data['id']) ? Article::where('id', $data['id']) : null;
	    $post = [
	    	'title' => $data['title'],
		    'text' => $data['text'],
		    'category_id' => $data['category'],
		    'updated_at' => date('Y-m-d H:i:s'),
		    ];
	    // если в запросе есть файл:
	    if ($request->hasFile('image_upload')) {
	    	// получить временное расположение файла
		    $temp_path = $request->file('image_upload')->getRealPath();
		    // если статья существует использовать путь существующего файла-изображения для сохранения,
		    // иначе создать новый путь
		    $save_path = $article ? $this->get_upload_path($article->first()->img_url) : $this->get_upload_path();
		    // создать url для изображения
		    $post['img_url'] = asset($save_path);
		    // изменить размер изображения и переместить в директорию
		    if (!$this->resizeImage(1140,800, $temp_path, $save_path)) {
			    throw new FileException('Неудалось загрузить файл');
		    }
	    }

	    try {
	    	// если статья существует обновить ее, иначе - создать новую
		    if ($article) {
			    $article->update($post);
		    }else {
			    // конвертируем текст в латиницу и создаем slug для url
			    $post['slug'] = str_slug($this->transliterate($data['title']), '-');
			    // получаем id текущего автора статьи
		        $post['author_id'] = Auth::user()->id;
			    $post['created_at'] = date('Y-m-d H:i:s');
		    	Article::insert($post);
		    }
	    }catch (Exception $e) {
		    Log::error('Ошибка : ' . $e->getMessage() . ' В строке: ' . $e->getLine() . ' В файле:' . $e->getFile());
		    if ($e instanceof QueryException || $e instanceof MassAssignmentException) {
			    throw new Exception('Ошибка базы данных:(');
		    }else {
		    	throw new \ErrorException($e);
		    }
	    }

		return response(['message' => 'Все в порядке, едем дальше!']);
    }

	/**
	 * Удаляет выбранную статьи и загруженный файл
	 *
	 * @param Request $request
	 * @throws Exception
	 */
    public function deleteArticle(Request $request)
    {
        $article = 	Article::where('id', $request->input('id'))->first();
	    $image = $this->get_upload_path($article->img_url);
		try{
			if ($article->delete()) {
				try {
					unlink($image);
					file_exists($image) ? Log::error('Файл не был удален корректно') : null ;
				}catch (\Exception $e) {
					Log::error('Ошибка : ' . $e->getMessage() . ' В строке: ' . $e->getLine() . ' В файле:' . $e->getFile());
					throw new FileException('Неудалось удалить файл');
				}
			}
		}catch (QueryException $e) {
			Log::error('Ошибка : ' . $e->getMessage() . ' В строке: ' . $e->getLine() . ' В файле:' . $e->getFile());
			throw new Exception('Не удалось удалить запись:( Попробуйте еще раз');
		}
    }


	/**
	 * @param $string
	 * @return mixed
	 */
    protected function transliterate($string) {
	    $roman = array("Sch","sch",'Yo','Zh','Kh','Ts','Ch','Sh','Yu','ya','yo','zh','kh','ts','ch','sh','yu','ya','A','B','V','G','D','E','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','','Y','','E','a','b','v','g','d','e','z','i','y','k','l','m','n','o','p','r','s','t','u','f','','y','','e');
	    $cyrillic = array("Щ","щ",'Ё','Ж','Х','Ц','Ч','Ш','Ю','я','ё','ж','х','ц','ч','ш','ю','я','А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Ь','Ы','Ъ','Э','а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','ь','ы','ъ','э');

	    return str_replace($cyrillic, $roman, $string);
    }


	/**
	 *  Создает уменьшенную JPEG копию загруженного файла  с сохранением пропорций
	 *
	 * @param $target_width    int     ширина исходящего изображения
	 * @param $target_heigth   int     высота исходящего изображения
	 * @param $original_path   string  путь к оригинальному изображению
	 * @param $target_path     string  путь для сохранения нового файла
	 *
	 * @return bool
	 */
	protected function resizeImage($target_width, $target_heigth, $original_path, $target_path)
	{
		// получаем размеры оригинального изображения
		list($orig_width, $orig_height) = getimagesize($original_path);
		// определяем формат оригинального изображения
		if (exif_imagetype($original_path) == IMAGETYPE_JPEG) {
			$image = imagecreatefromjpeg($original_path);
		}elseif(exif_imagetype($original_path) == IMAGETYPE_PNG) {
			$image = imagecreatefrompng($original_path);
		}
		// определяем соотношение стороно оригинального изображения
		$orig_ratio = $orig_width / $orig_height;
		// Определяем горизонтальное или вертикальное исходящее изображение
		// и корректируем по необходимости параметры исходящего изображения
		if ($target_width / $target_heigth > $orig_ratio) {
			$target_width = $target_heigth * $orig_ratio;
		} else {
			$target_heigth = $target_width / $orig_ratio;
		}
		// создаем пустой ресурс для записи нового изображения
		$image_p = imagecreatetruecolor($target_width, $target_heigth);
		// копируем данные оригинального изображения в созданный ресурс
		imagecopyresampled($image_p, $image, 0, 0, 0, 0, intval($target_width), intval($target_heigth), $orig_width, $orig_height);
		// создаем новое изображение в указанном местоположении и возвращаем результат операции
		return imagejpeg($image_p, $target_path, 100);
	}

	/**
	 * Проверяет существует ли указанный каталог для загрузки изображений
	 * и пытается его создать если отсутствует.
	 *
	 * @return string       путь к каталогу для загрузки изображений
	 */
	protected function get_upload_path($existing_name = NULL)
	{
		$dir = 'img/articles';
		if (!file_exists(public_path($dir))) {
			try {
				mkdir(public_path($dir));
			}catch (Exception $e) {
				Log::error('Ошибка : ' . $e->getMessage() . ' В строке: ' . $e->getLine() . ' В файле:' . $e->getFile());
				throw new FileException('Неудалось создать каталог для загрузки изображений');
			}
		}
		// Если указан $existing_name, то использовать его для формирования ссылки
		return  $existing_name ? $dir . $this->extract_filename($existing_name) : $dir . '/' . str_random(10) . '.jpg';
	}


	/**
	 * Достает имя файла изображения и его расширение из ссылки
	 * (filename.jpg)
	 * @param $path     ссылка на файл (url или абсолютная)
	 * @return string   имя файла и расширение
	 */
	protected function extract_filename($path)
	{
		$filename = [];
		preg_match('/\/([0-9a-zA-Z])+\.[jpg]+/', $path, $filename);

		return $filename[0];
	}
}
