<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Article;
use App\Category;

// Возвращает постранично список всeх статей и их категорий для авторизированного пользователя
// и возвращает первые 6 статей и показывает кнопку подгрузки для Гостя.
Route::get('/', function () {
    return view('news_list', [
    	'articles' => Auth::check() ? Article::where('author_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(12) :
		    Article::orderBy('created_at', 'desc')->take(6)->get(),
	    'categories' => Category::all(),
    ]);
})->name('home');

// Возвращает страницу выбранной статьи
Route::get('article/{slug}', function ($slug) {
	$article = Article::where('slug', '=', $slug);
	$article->increment('views');
	return view('news_article', [
		'article' => $article->first(),
	]);
})->name('get_article');

// Обрабатывает запрос на получение дополнительных статей (кнопка "Показать больше")
// Возвращает в формате JSON 6 статей из БД со сдвигом на уже существующее в DOM браузера количество статей
Route::post('article/load_more', function (Request $request) {
	$rows = Article::orderBy('created_at', 'desc');
	$offset = $request->input('offset');
	$remain = $rows->count() - $offset;
	$articles = $rows->skip($offset)->take(6)->get()->map(function ($item, $index) {
		$article = new StdClass();
		$article->url = url('article/' . $item->slug);
		$article->title = $item->title;
		$article->category = $item->category->name;
		$article->category_slug = $item->category->slug;
		$article->img_url = $item->img_url;
		$article->text = strip_tags(str_limit($item->text, 120));
		$article->date = date('d/m/y', strtotime($item->created_at));
		$article->views = $item->views;

		return $article;
	})->toJson();

	return response()->json(['articles' => $articles, 'remain' => $remain]);
})->name('load_more');

// Обрабатывает запрос на регистрацию
Route::post('signup', 'Auth\AuthController@register')->name('register');
// Обрабатывает запрос на авторизацию
Route::post('login', 'Auth\AuthController@login')->name('login');

// Группа запросов, которые может делать авторизированный пользователь-администратор
Route::group(['prefix'=> 'admin', 'middleware' => 'auth'], function () {
	// Получение формы создания новой статьи
	Route::post('article/get', function (Request $request){
		return response()->view('admin.admin_article_form', [
			'article' => Article::where('id', $request->input('id'))->first(),
			'categories' => Category::all(),
		])->header('Content-Type', 'text/html; charset=utf-8');
	})->name('edit_article');
	// Запрос на создание/обновления статьи в БД
	Route::post('article/post', 'ArticleController@postArticle')->name('post_article');
	// Запрос на удаление статьи
	Route::post('article/remove', 'ArticleController@deleteArticle')->name('delete_article');
	// Обрабатывает запрос на деавторизацию
	Route::get('logout', function () {
		Auth::logout();
		return redirect('/');
	})->name('logout');
});


