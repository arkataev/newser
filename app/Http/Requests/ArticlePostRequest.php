<?php

namespace App\Http\Requests;

use App\Article;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class ArticlePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    	return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
	        'text' => 'required',
	        'category' => 'required',
        ];
    }

    public function messages()
    {
	    return [
		    'title.required' => 'Не указан загаловок статьи',
		    'title.unique' => 'Статья с таким заголовком уже существует',
		    'title.max' => 'Загаловок превышает допустимую длину',
		    'text.required' => 'Не указано содержание статьи',
		    'category.required' => 'Не выбрана категория новости',
		    'image_upload.required' => 'Не выбрано главное изображение статьи',
		    'image_upload.image' => 'Выбранный файл не является допустимым изображением',
		    'image_upload.mimes' => 'Выбран недопустимый формат изображения (используйте формат JPG, PNG)',
		    'image_upload.dimensions' => 'Выбранный файл имеет недопустимые размеры (Ширина >= 1920px, Длина >=1080px)',
	    ];
    }

    public function validate()
    {
	    if ($this->file('image_upload')) {
		    if (!$this->file('image_upload')->isValid()) { throw new FileException('Возникла ошибка при загрузке 
		    изображения, проверьте тип и размер файла'); };
	    }

	    $validator = Validator::make($this->all(), $this->rules(), $this->messages());
	    // Добавить проверку загружаемого изображения, только если у статьи нет сохраненного изображения
	    $validator->sometimes('image_upload', 'required|image|dimensions:min_width=1920,min_height=1080|mimes:jpeg,png,jpg',
		    function ($input) {
				$image = Article::where('id', $input->id)->first() ? Article::where('id', $input->id)->first()->img_url : false;
			    $file = isset($input->image_upload) ? $input->image_upload  : false;
			    return ($image && $file) || (!$image && $file) || (!$image && !$file) ? true : false;
	        });
		// Добавить проверку заголовка статьи, только если в БД есть другие статьи с похожим заголовком
	    $validator->sometimes('title', 'unique:articles,title', function ($input) {
	    	return Article::where([['title', $input->title], ['id', '<>', $input->id]])->first();
	    });
	    $validator->fails() ? $this->failedValidation($validator) : null;
    }
}
