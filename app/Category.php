<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * Основная модель Категории статей
 *
 * @package App
 */
class Category extends Model
{
	/**
	 * Специальный метод Laravel для определения зависимостей в БД
	 * Определяет, что данное категории может принадлежать несколько статей
	 * Позволяет вызывать модели статей, которые принадлежат данной категории
	 * с помощью $categoryObj->articles...
	 *
	 * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
    public function articles()
    {
    	return $this->hasMany('App\Article');
    }
}
