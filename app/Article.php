<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Article
 * Основная модель новостной статьи
 * @package App
 */
class Article extends Model
{
	/**
	 * Специальный метод Laravel для определения зависимостей в БД
	 * Определяет, что статья может принадлежать одному пользователю
	 * Позволяет вызывать модель пользователя, к которой принадлежит модель данной статьи
	 * с помощью $articleObj->user...
	 *
	 * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo('App\User',  'author_id', 'id');
	}

	/**
	 * Специальный метод Laravel для определения зависимостей в БД
	 * Определяет, что статья может принадлежать одной категории
	 * Позволяет вызывать модель категории, к которой принадлежит модель данной статьи
	 * с помощью $articleObj->category...
	 *
	 * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo('App\Category');
	}
}
