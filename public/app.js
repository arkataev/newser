/**
 * Created by arkat on 22.07.2016.
 */

$(window).load(
	function () {

		// Projects Isotope filter
		if(typeof Isotope !== 'undefined') {
			var $filter  = $('#news_list').imagesLoaded(function() {
				$filter.isotope({
					itemSelector: '.news_item',
					layoutMode: 'masonry',
					percentPosition: true,
					stagger: 30,
				})
			});

			// Projects filter
			$('.filter_categories').on( 'click', 'a', function(e) {
				e.preventDefault();
				var filterValue = $(this).attr('id');
				$filter.isotope({ filter: filterValue });
			});
		}

		var $adminModal = $('#admin_article_modal');
		// Обновление статьи
		$('.article_update').click(getArticle);
		// Удаление статьи
		$('.article_remove').click(deleteArticle);
		// Получение пустой формы для новой сттьи
		$('#add_article').click(getArticle);
		// Загрузить больше статей
		$('#load_more').click(loadMore);
		// Форма Входа и Регистрации
		$('.form_submit').click(function(e) {
			e.preventDefault();
			var $form = $(this).parents('.modal');
			var $formErrors = $form.find('.errors');

			collectInputDataAndSend($form, $(this).data('target')).fail(function(data) {
				// потрясти формой
				shake($form);
				// очистить блок ошибок
				$formErrors.html('');
				// достать сообщения из ответа сервера и добавить в блок ошибок
				getResponseMessages(data.responseJSON).forEach(function (item) {
					$formErrors.prepend($('<li>' + item + '</li>'))
				});
				$formErrors.show();
			}).done(function () {
				$form.modal('hide');
				location.reload();
			});
		});
		// Закрытие окна уведомлений
		$('.alert_close').click(function () { $('#alert-container').fadeOut('fast'); $('.overlay').hide(); });
		// Вход в личный кабинент
		$('#login').click(function (e) {
			e.preventDefault();
			var $form = $('#admin_login');
			var $formErrors = $form.find('.errors');
			$formErrors.hide();
			$form.modal('show');
		});
		// Регистрация нового пользователя
		$('#user_signup').click(function (e) {
			e.preventDefault();
			var $form = $('#signup');
			var $formErrors = $form.find('.errors');
			$formErrors.hide();
			$form.modal('show');
		});
		// При открытии модального окна редактирования статьи
		$adminModal.on('show.bs.modal', function () {
			// Событие на загрузку изображения
			$('#article').find('.upload').change(articleImageUpload);
			// Событие на сохранение статьи
			$('.article_save').click(postArticle);
		});
		// При закрытии окна редактирования проекта
		$adminModal.on('hide.bs.modal', function () {
			// удаляем объект CKEDITOR
			CKEDITOR.instances.article_text ? CKEDITOR.instances.article_text.destroy(true) : null;
			// убираем информацию предыдущего окна
			$('#admin_article_modal').find('.modal-content').html('');
		});
	}
);

// Показывает модальное окно загрузчика
function showLoader() {
	let loader = $('#loader');
	loader.css('top', $(window).scrollTop() + 200 +  "px");
	loader.fadeIn('fast');

	return loader;
}

/**
 * Собирает данные формы и отправляет на сервер череp AJAX-запрос
 * В случает ошибки отображает модальное окно с сообщением
 */
function collectInputDataAndSend($form,target) {
	var data = {};
	$form.find('input').each(function (index) {
		data[$(this).prop('name')] = $(this).val();
	});

	return send(target, createFormData(data));
}


// Анимация трясения блока
// @author http://bradleyhamilton.com/projects/shake/index.html
function shake(div) {
	var interval = 100;
	var distance = 10;
	var times = 4;
	for(var iter=0; iter < (times + 1); iter++) {
		$(div).animate({
			left: iter % 2 == 0 ? distance : distance * -1,
		}, interval);
	}
	$(div).animate({left : 0}, interval);
}

// Возвращает модальное окно с пустой или заполненной формой статьи
function getArticle(e) {
	e.preventDefault();
	var loader = showLoader();
	var $modal = $('#admin_article_modal');

	send($(this).prop('href'), createFormData({'id' : $(this).data('article_id')})).done(function (data) {
		$modal.find('.modal-content').append(data);
		CKEDITOR.replace('article_text');
		loader.fadeOut('fast');
		$modal.modal('show');
		$('.article_save').click(buttonLoading);
	})
}

function postArticle() {
	var article = {};
	var file = $('input[type=file]')[0];

	article.id = $(this).data('article_id');
	article.title = $('input[name=title]').val();
	article.category = $('#article_category').val();
	article.text = CKEDITOR.instances['article_text'].getData();
	article.image_upload = file ? file.files[0] : null;

	send($(this).data('target'), createFormData(article)).always(function(data) {
		buttonRestore('article_save', 'Сохранить');
		showModalResponse(data);
	}).done(function () {
		$('.alert_close').click(function () {
			$('#admin_article_modal').modal('hide');
			location.reload();
		})
	});
}

function deleteArticle(e) {
	e.preventDefault();
	var loader = showLoader();
	send($(this).prop('href'), createFormData({'id' : $(this).data('article_id')})).always(function() {
		loader.fadeOut('fast');
		location.reload();
	});
}

/**
 * Кнопка "Загрузить больше"
 * Асинхронно загружает следующие 6 элементов из БД и добавляет их в список
 */
function loadMore() {
	var loader = showLoader();
	send($(this).data('target'), createFormData( {'offset': $('.news_item').length}) ).done(function (data) {
		// если осталось 6 или меньше объектов в БД - скрыть кнопку загрузки
		let remainArticles = data.remain;
		remainArticles <= 6 ? $('#load_more').hide() : undefined;
		// Добавить новые статьи в общий список
		JSON.parse(data.articles).forEach(function (item) {
			var $item = $('<li class="news_item" id="' + item.category_slug + '">' +
				'<a href="' + item.url + '" target="_blank">' +
				'<div class="news_item_block">' +
				'<div class="image_wrapper">' +
				'<span class="news_item_label">' + item.category + '</span>' +
				'<img src="' + item.img_url + '" alt="">' +
				'</div>' +
				'<div class="news_item_content">' +
				'<h1 class="news_item_header">' + item.title + '</h1>' +
				'<p class="news_item_text">' + item.text + '</p>' +
				'<div class="news_item_extra">' +
				'<div class="row">' +
				'<div class="col-md-8"><span class="news_item_date">' + item.date + '</span></div>' +
				'<div class="col-md-4">' +
				'<span class="views icon-views">' + item.views + '</span>' +
				'<span class="comments icon-comment">--</span>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</a>' +
				'</li>');
			$('#news_list').append($item).isotope( 'appended', $item);
		});
		loader.fadeOut('fast');
	})
}



function loadImageFromFile(file, data={}) {
	var img = new Image();
	// set id of modal window to load image into
	data.modal  !== undefined ? img.dataset.modal = data.modal  : null;
	// set id of target object to load image data into
	data.target !== undefined ? img.dataset.target = data.target : null;
	// set new thumbnail class
	img.classList.add("img_preview");
	// attach file to thumbnail object
	img.file = file;
	// create new object URL
	img.src = window.URL.createObjectURL(file);
	// when image is loaded release object URL and display image
	img.onload = function () {
		this.dataset.width = this.naturalWidth;
		this.dataset.height = this.naturalHeight;
		window.URL.revokeObjectURL(this.src);
	};

	return img;
}


/**
 * Создает заполненный объект FormData из свойств объекта на входе
 *
 * @param data      Объект свойства, которого нужно преобразовать в форму для отправки
 * @returns {*}
 */
function createFormData(data) {
	var form = new FormData();
	for (var key in data) {
		data[key] ? form.append(key, data[key]) : null;
	}

	return form;
}

function articleImageUpload(upload) {
	var file = $(upload.target)[0].files.item(0);
	var thumb = loadImageFromFile(file);
	$('#article_img').attr('src', thumb.src);
}



function getResponseMessages(response) {
	var messages = [];
	for (var key in response) {
		if (typeof response[key] == 'object') {
			response[key].forEach(function (item) {
				messages.push(item);
			})
		}else {
			messages.push(response[key]);
		}
	}

	return messages;
}

function showModalResponse(data) {
	$('#alert-container .message li').detach();
	var messageClass = data.status && data.status == 422 ? 'icon-item-remove' : 'icon-ok-circled';
	var response = data.responseJSON || data;
	var messages = getResponseMessages(response);
	$('#message_type').attr('class', messageClass);
	messages.forEach(function (item) {
		$('#alert-container .message').append($('<li>' + item + '</li>'));
	})
	$('#alert-container').css('top', $(window).scrollTop() + 200 +  "px");
	$('#alert-container').fadeIn('fast');
	$('.overlay').show();
}

// Изменение кнопки на режим обновления данных
function buttonLoading() {
	$(this).children('.loader-inner').html('' +
		'<div></div>' +
		'<div></div>' +
		'<div></div>' +
		'');
	$(this).prop('disabled', true)
}
// Возвращение кнопки в исходное состояние
function buttonRestore(btn_class, name) {
	let $button  = $('.' + btn_class);
	$button.children('.loader-inner').html(name);
	$button.prop('disabled', false);
}
// Отправка AJAX - запроса к серверу
function send(url, data) {
	return $.ajax({
		'url' : url,
		'headers': {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') // Laravel security token
		},
		'method' : 'post',
		'data' : data,
		'cache' : false,
		'processData' : false,
		'contentType': false
	});
}

// Фикс для открытия нескольких модальных окон Bootstrap одновременно на одной странице
// @author http://stackoverflow.com/questions/19305821/multiple-modals-overlay
$(document).on('show.bs.modal', '.modal', function () {
	var zIndex = 1040 + (10 * $('.modal:visible').length);
	$(this).css('z-index', zIndex);
	setTimeout(function() {
		$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	}, 0);
});

// Фикс для корректной работы скролла при открытии нескольких модальных окон на одной странице
$(document).on('hidden.bs.modal', '.modal', function () {
	$('.modal:visible').length && $(document.body).addClass('modal-open');
});

// фикс для полей ввода CKEditor, которые неактивны при вызове из модального окна
// источник http://jsfiddle.net/pvkovalev/4PACy/
$.fn.modal.Constructor.prototype.enforceFocus = function () {
	modal_this = this
	$(document).on('focusin.modal', function (e) {
		if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
			// add whatever conditions you need here:
			&&
			!$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
			modal_this.$element.focus()
		}
	})
};