<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 22.07.2016
 */
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="ru" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="ru" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="ru" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="ru" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="ru" class="no-js"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	<meta name="author" content="">
	<meta name="keywords" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('app.css') }}">
	<link rel="stylesheet" href="{{ asset('fontello/css/fontello.css') }}">
	<link rel="stylesheet" href="{{ asset('loaders.css/loaders.min.css') }}">
	<title>Новостная лента Newser | @yield('title')</title>
</head>
<body>
<div class="overlay"></div>
<div id="admin_article_modal" class="modal fade" tabindex="-2" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"></div>
	</div>
</div>
<div id="admin_login" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="{{route('login')}}" method="post">
				<div class="form-group">
					<input type="email" name="email" id="email" placeholder="Электропочта" required>
				</div>
				<div class="form-group">
					<input type="password" name="password" placeholder="Пароль" required>
				</div>
				{{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
				<button class="form_submit" data-target="{{route('login')}}" type="submit">Войти</button>
				<ul class="errors"></ul>
			</form>
		</div>
	</div>
</div>
<div id="signup" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<form action="{{route('register')}}" method="post">
				<div class="form-group">
					<input type="text" name="name" id="name" placeholder="Имя">
				</div>
				<div class="form-group">
					<input type="email" name="email" placeholder="Электропочта">
				</div>
				<div class="form-group">
					<input type="password" name="password" placeholder="Пароль">
				</div>
				<div class="form-group">
					<input type="password" name="password_confirmation" placeholder="Подтвердите Пароль">
				</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<button type="submit" class="form_submit" data-target="{{route('register')}}">Регистрация</button>
				<ul class="errors"></ul>
			</form>
		</div>
	</div>
</div>
<div id="loader">
	<div class="loader_frame">
		<div class="loader-inner ball-scale-ripple-multiple"><div></div><div></div><div></div></div>
		<p>Получаю данные...</p>
	</div>
</div>
<div id="alert-container">
	<div class="alert_error">
		<span id="message_type"></span>
		<ul class="message"></ul>
	</div>
	<a href="#" class="alert_close">Закрыть</a>
</div>
@yield('content')
<script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="//cdn.ckeditor.com/4.5.10/full/ckeditor.js"></script>
<script src="https://npmcdn.com/isotope-layout@3.0.1/dist/isotope.pkgd.min.js"></script>
<script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
<script src="{{asset('app.js')}}"></script>
</body>
</html>
