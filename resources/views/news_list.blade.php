<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 22.07.2016
 *
 * Список новостей с постраничной разбивкой
 */
?>
@extends('layouts.layout')
@section('content')
		<div class="container">
			<nav class="navabar">
				<div class="filter_categories">
					<a class='active' href="#" id="*" >#Все</a>
					@foreach($categories as $category)
						<a href="#" id="#{{$category->slug}}">#{{$category->name}}</a>
					@endforeach
				</div>
				<ul id="admin_login">
					@if(Auth::check())
						<li id="logout"><a href="{{route('logout')}}">Выход</a></li>
					@else
						<li id="login"><a href="#">Вход</a></li>
						<li id="user_signup"><a href="#">Регистрация</a></li>
					@endif
				</ul>
				<div id="author_tag" >
					<p>сделано RedDev</p>
					<a href="https://bitbucket.org/arkataev" target="_blank"><span class="icon-bitbucket">Bitbucket</span></a>
					<a href="https://github.com/arkataev" target="_blank"><span class="icon-github-circled">GitHub</span></a>
			</nav>
			<div class="row">
				<div class="col-lg-12">
					<div class="wrapper">
						<ul id="news_list">
							@if(Auth::check())
								<li class="news_item first">
									<a href="{{route('edit_article')}}" id="add_article">
										<div class="add">
											<span class="icon-plus"></span>
										</div>
									</a>
								</li>
							@endif
							@foreach($articles as $article)
								<li class="news_item" id="{{$article->category->slug}}">
									<a href="{{route('get_article', ['slug' => $article->slug])}}" target="_blank">
										<div class="news_item_block">
											<div class="image_wrapper">
												<span class="news_item_label">{{$article->category->name}}</span>
												<img src="{{$article->img_url}}" alt="{{$article->slug}}">
											</div>
											<div class="news_item_content">
												<h1 class="news_item_header">{{$article->title}}</h1>
												<p class="news_item_text">{{ strip_tags(str_limit($article->text, 120)) }}</p>
												<div class="news_item_extra">
													<div class="row">
														<div class="col-md-8">
															<span class="news_item_date">{{date('d/m/y', strtotime($article->created_at))}}</span>
														</div>
														<div class="col-md-4">
															<span class="views icon-views">{{$article->views}}</span>
															<span class="comments icon-comment">--</span>
														</div>
													</div>
												</div>
											</div>
										</div>
										@if(Auth::check())
										<div class="tint">
											<ul class="admin_controlls">
												<li><a href="{{route('get_article', ['slug' => $article->slug])}}" target="_blank"><span class="icon-views"></span></a></li>
												<li><a href="{{route('edit_article')}}" class="article_update" data-article_id="{{$article->id}}"><span class="icon-pencil"></span></a></li>
												<li><a href="{{route(('delete_article'))}}" class="article_remove" data-article_id="{{$article->id}}"><span class="icon-item-remove"></span></a></li>
											</ul>
										</div>
										@endif
									</a>
								</li>
							@endforeach
						</ul>
						@if(!Auth::check())
							<button id="load_more" data-target="{{route('load_more')}}">Загрузить еще...</button>
						@else
							{{$articles->links()}}
						@endif
					</div>
				</div>
			</div>
		</div>
@endsection