<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 22.07.2016
 *
 * Панель управления новостями
 */
?>
<div id="article">
	<div class="row">
		<div class="col-md-12">
			<div class="image_wrapper">
				@if($article)
					<img id="article_img" src="{{!$article ? null : $article->img_url}}" alt="{{!$article ? null : $article->slug}}">
					<div class="tint">
						<div class="add">
							<span class="icon-plus"><input type="file" name="article_img" class="upload"></span>
						</div>
						<p>Загрузить изображение</p>
					</div>
				@else
					<img id="article_img" src="" alt="">
					<div class="tint">
						<div class="add">
							<span class="icon-plus"><input type="file" name="article_img" class="upload"></span>
						</div>
						<p>Загрузить изображение</p>
					</div>
				@endif
			</div>
			<div class="form-group">
				<input type="text" name="title" placeholder="Основной заголовок (не более 50 символов)" value="{{!$article ? null : $article->title}}" maxlength="50">
			</div>
			<div class="form-group">
				<textarea rows="3" id="article_text" name="text">{{!$article ? null : $article->text}}</textarea>
			</div>
			<div class="form-group">
				<select name="article_category" id="article_category">
					@if(!$article)
						@foreach($categories as $category)
							<option value="{{$category->id}}">{{$category->name}}</option>
						@endforeach
					@else
						@foreach($categories as $category)
							<option value="{{$category->id}}" {{$category->id == $article->category->id ? 'selected' : null}}>{{$category->name}}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul id="control-buttons">
						<li><button class="article_save" data-target="{{route('post_article')}}" data-article_id="{{!$article ? null : $article->id}}"><div class="loader-inner ball-pulse">Сохранить</div></button></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

