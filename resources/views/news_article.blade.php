<?php
/**
 * @author arkataev
 * @web https://github.com/arkataev
 * @date: 22.07.2016
 *
 * Страница новости
 */
?>
@extends('layouts.layout')
@section('title')
	{{$article->title}}
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<article id="news_article">
				<a href="{{route('home')}}"><span class="return"><<<</span></a>
				<div class="image_wrapper" style="background: url('{{$article->img_url}}')"></div>
				<div class="row">
					<div class="col-md-8 col-md-offset-1 col-sm-12">
						<div class="article_content">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-6 col-md-offset-9 col-sm-offset-8 col-xs-offset-6">
									<p id="category">{{$article->category->name}}</p>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-10 col-xs-10 col-sm-offset-1 col-xs-offset-1">
									<h1>{{$article->title}}</h1>
									<div class="article_text">
										{!! $article->text !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-10 col-xs-10 col-sm-offset-1 col-xs-offset-1">
									<div class="article_extra">
										<span id="author">{{$article->user->name}}</span>
										<span id="date">{{date('d/m/y', strtotime($article->created_at))}}</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
</div>
@endsection